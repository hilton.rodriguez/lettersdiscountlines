package com.bbva.pzic.lettersdiscountlines;

import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF41;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF42;
import com.bbva.pzic.lettersdiscountlines.util.mappers.ObjectMapperHelper;

import java.io.IOException;

public class FormatoRif4Stubs {

    private ObjectMapperHelper objectMapperHelper = ObjectMapperHelper.getInstance();

    private  static final FormatoRif4Stubs INSTANCE = new FormatoRif4Stubs();

    private FormatoRif4Stubs(){

    }

    public static FormatoRif4Stubs getInstance(){
        return INSTANCE;
    }

    public FormatoRIMRF41 getFormatoRIMRF41() throws IOException{
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader()
            .getResourceAsStream("mock/FormatoRIMRF41ResponseBackend.json"), FormatoRIMRF41.class);
    }

    public FormatoRIMRF42 getFormatoRIMRF42() throws IOException{
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("mock/FormatoRIMRF42ResponseBackend.json"), FormatoRIMRF42.class);
    }
}
