package com.bbva.pzic.lettersdiscountlines;

import com.bbva.pzic.lettersdiscountlines.business.dto.InputListHistoricalLettersSummaries;
import com.bbva.pzic.lettersdiscountlines.util.mappers.ObjectMapperHelper;

public class EntityMock {

    public static final String BUSINESS_ID = "00000655";

    private final ObjectMapperHelper objectMapperHelper = ObjectMapperHelper.getInstance();
    private static final EntityMock INSTANCE = new EntityMock();

    public static EntityMock getInstance(){
        return INSTANCE;
    }

    public InputListHistoricalLettersSummaries getInputListHistoricalLettersSummaries(){
        InputListHistoricalLettersSummaries input = new InputListHistoricalLettersSummaries();
        input.setBusinessId(BUSINESS_ID);
        return input;
    }
}
