package com.bbva.pzic.lettersdiscountlines.business;

import com.bbva.pzic.lettersdiscountlines.business.dto.InputListHistoricalLettersSummaries;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;

import java.util.List;

public interface ISrvInputLettersDiscountLines {
    List<HistoricalLettersSummaries> listHistoricalLettersSummaries(InputListHistoricalLettersSummaries input);
}
