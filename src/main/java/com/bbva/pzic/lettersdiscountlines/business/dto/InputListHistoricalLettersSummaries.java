package com.bbva.pzic.lettersdiscountlines.business.dto;

import javax.validation.constraints.NotNull;

public class InputListHistoricalLettersSummaries {

    @NotNull(groups = ValidationGroup.ListHistoricalLettersSummaries.class)
    private String businessId;

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }
}
