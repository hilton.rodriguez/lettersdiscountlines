package com.bbva.pzic.lettersdiscountlines.business.impl;

import com.bbva.pzic.lettersdiscountlines.business.ISrvInputLettersDiscountLines;
import com.bbva.pzic.lettersdiscountlines.business.dto.InputListHistoricalLettersSummaries;
import com.bbva.pzic.lettersdiscountlines.business.dto.ValidationGroup;
import com.bbva.pzic.lettersdiscountlines.dao.ILettersDiscountLinesDAO;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Validator;
import java.util.List;

@Component
public class SrvInputLettersDiscountLines implements ISrvInputLettersDiscountLines {

    private static final Log LOG = LogFactory.getLog(SrvInputLettersDiscountLines.class);

    @Autowired
    private Validator validator;

    @Autowired
    private ILettersDiscountLinesDAO lettersDiscountLinesDAO;

    @Override
    public List<HistoricalLettersSummaries> listHistoricalLettersSummaries(final InputListHistoricalLettersSummaries input) {
        LOG.info("... Invoking method SrvInputLettersDiscountLines.listHistoricalLettersSummaries ...");
        LOG.info("... Validating listHistoricalLettersSummaries input parameter ...");
        validator.validate(input, ValidationGroup.ListHistoricalLettersSummaries.class);
        return lettersDiscountLinesDAO.lisHistoricalLettersSummaries(input);
    }
}
