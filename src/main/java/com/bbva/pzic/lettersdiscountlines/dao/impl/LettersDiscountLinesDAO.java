package com.bbva.pzic.lettersdiscountlines.dao.impl;

import com.bbva.pzic.lettersdiscountlines.business.dto.InputListHistoricalLettersSummaries;
import com.bbva.pzic.lettersdiscountlines.dao.ILettersDiscountLinesDAO;
import com.bbva.pzic.lettersdiscountlines.dao.tx.TxListHistoricalLettersSummaries;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class LettersDiscountLinesDAO implements ILettersDiscountLinesDAO {

    private static final Log LOG = LogFactory.getLog(LettersDiscountLinesDAO.class);

    @Resource(name = "txListHistoricalLettersSummaries")
    private TxListHistoricalLettersSummaries txListHistoricalLettersSummaries;

    @Override
    public List<HistoricalLettersSummaries> lisHistoricalLettersSummaries(InputListHistoricalLettersSummaries input) {
        LOG.info("... Invoking method LettersDiscountLinesDAO.listHistoricalLettersSummaries ...");
        return txListHistoricalLettersSummaries.perform(input);
    }
}
