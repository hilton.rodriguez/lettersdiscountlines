package com.bbva.pzic.lettersdiscountlines.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.pzic.lettersdiscountlines.business.dto.InputListHistoricalLettersSummaries;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.*;
import com.bbva.pzic.lettersdiscountlines.dao.tx.mapper.ITxListHistoricalLettersSummariesMapper;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.DoubleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

@Component
public class TxListHistoricalLettersSummaries extends DoubleOutputFormat<InputListHistoricalLettersSummaries, FormatoRIMRF40, List<HistoricalLettersSummaries>, FormatoRIMRF41, FormatoRIMRF42> {
    @Resource(name = "txListHistoricalLettersSummariesMapper")
    private ITxListHistoricalLettersSummariesMapper mapper;

    @Autowired
    public TxListHistoricalLettersSummaries(@Qualifier("transaccionRif4") InvocadorTransaccion<PeticionTransaccionRif4, RespuestaTransaccionRif4> transaction) {
        super(transaction, PeticionTransaccionRif4::new, ArrayList::new, FormatoRIMRF41.class, FormatoRIMRF42.class);
    }

    @Override
    protected FormatoRIMRF40 mapInput(InputListHistoricalLettersSummaries inputListHistoricalLettersSummaries) {
        return mapper.mapIn(inputListHistoricalLettersSummaries);
    }

    @Override
    protected List<HistoricalLettersSummaries> mapFirstOutputFormat(FormatoRIMRF41 formatoRIMRF41, InputListHistoricalLettersSummaries inputListHistoricalLettersSummaries, List<HistoricalLettersSummaries> historicalLettersSummariesList) {
        return mapper.mapOut1(formatoRIMRF41);
    }

    @Override
    protected List<HistoricalLettersSummaries> mapSecondOutputFormat(FormatoRIMRF42 formatoRIMRF42, InputListHistoricalLettersSummaries inputListHistoricalLettersSummaries, List<HistoricalLettersSummaries> historicalLettersSummariesList) {
        return mapper.mapOut2(formatoRIMRF42, historicalLettersSummariesList);
    }
}
