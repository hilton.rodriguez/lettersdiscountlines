package com.bbva.pzic.lettersdiscountlines.dao.model.rif4;

import com.bbva.jee.arq.spring.core.host.*;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;


/**
 * <p>Transacci&oacute;n <code>RIF4</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionRif4</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionRif4</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.RIF4.D1201029.TXT
 * RIF4LISTADO INDICADORES DE NEGOCIO     RI        RI2C00F4     01 RIMRF40             RIF4  NS0000CNNNNN    SSTN    C   NNNNSNNN  NN                2020-09-22XP90749 2020-10-2811.39.18XP90749 2020-09-22-19.46.18.743577XP90749 0001-01-010001-01-01
 * 
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.RIMRF40.D1201029.TXT
 * RIMRF40 �CODIGO DE CLIENTE             �F�01�00008�01�00001�CODCENT�CODIGO DE CLIENTE   �A�008�0�R�        �
 * 
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.RIMRF41.D1201029.TXT
 * RIMRF41 �SALIDA DEL INDICADOR          �X�02�00058�01�00001�ID     �IDENTIFICADOR UNICO �A�026�0�S�        �
 * RIMRF41 �SALIDA DEL INDICADOR          �X�02�00058�02�00027�EFEDES �DESCRIPCION DEL INDI�A�032�0�S�        �
 * 
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.RIMRF42.D1201029.TXT
 * RIMRF42 �SALIDA DEL INDICADOR          �X�03�00013�01�00001�PEREFEL�PERIODO EFEC.LETRAS �N�003�0�S�        �
 * RIMRF42 �SALIDA DEL INDICADOR          �X�03�00013�02�00004�UNIEFEL�UNIDAD EFEC.LETRAS  �A�007�0�S�        �
 * RIMRF42 �SALIDA DEL INDICADOR          �X�03�00013�03�00011�POREFE �PORCENT.EFEC.LETRAS �N�003�0�S�        �
 * 
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.RIF4.D1201029.TXT
 * RIF4RIMRF41 RINCRF41RI2C00F41S58                           XP90749 2020-09-22-20.53.15.346840XP90749 2020-10-28-11.37.29.556720
 * RIF4RIMRF42 RINCRF42RI2C00F42S10                           XP90749 2020-09-22-20.54.23.288460XP90749 2020-09-23-16.59.26.727104
 * 
</pre></code>
 * 
 * @see RespuestaTransaccionRif4
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "RIF4",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionRif4.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoRIMRF40.class})
@RooJavaBean
@RooSerializable
public class PeticionTransaccionRif4 implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}