package com.bbva.pzic.lettersdiscountlines.dao.model.rif4;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>RIF4</code>
 * 
 * @see PeticionTransaccionRif4
 * @see RespuestaTransaccionRif4
 */
@Component
public class TransaccionRif4 implements InvocadorTransaccion<PeticionTransaccionRif4,RespuestaTransaccionRif4> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionRif4 invocar(PeticionTransaccionRif4 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionRif4.class, RespuestaTransaccionRif4.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionRif4 invocarCache(PeticionTransaccionRif4 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionRif4.class, RespuestaTransaccionRif4.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}