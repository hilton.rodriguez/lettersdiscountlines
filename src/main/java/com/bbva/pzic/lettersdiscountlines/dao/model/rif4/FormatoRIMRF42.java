package com.bbva.pzic.lettersdiscountlines.dao.model.rif4;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;


/**
 * Formato de datos <code>RIMRF42</code> de la transacci&oacute;n <code>RIF4</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "RIMRF42")
@RooJavaBean
@RooSerializable
public class FormatoRIMRF42 {
	
	/**
	 * <p>Campo <code>PEREFEL</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 1, nombre = "PEREFEL", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer perefel;
	
	/**
	 * <p>Campo <code>UNIEFEL</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "UNIEFEL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 7, longitudMaxima = 7)
	private String uniefel;
	
	/**
	 * <p>Campo <code>POREFE</code>, &iacute;ndice: <code>3</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 3, nombre = "POREFE", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer porefe;
	
}