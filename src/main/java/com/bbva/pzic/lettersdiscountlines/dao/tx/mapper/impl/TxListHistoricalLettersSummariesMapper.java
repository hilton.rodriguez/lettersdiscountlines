package com.bbva.pzic.lettersdiscountlines.dao.tx.mapper.impl;

import com.bbva.pzic.lettersdiscountlines.business.dto.InputListHistoricalLettersSummaries;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF40;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF41;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF42;
import com.bbva.pzic.lettersdiscountlines.dao.tx.mapper.ITxListHistoricalLettersSummariesMapper;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalParameters;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.InformationPeriod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TxListHistoricalLettersSummariesMapper implements ITxListHistoricalLettersSummariesMapper {

    private static final Log LOG = LogFactory.getLog(TxListHistoricalLettersSummariesMapper.class);


    @Override
    public FormatoRIMRF40 mapIn(final InputListHistoricalLettersSummaries input) {
        LOG.info("... called method TxListHistoricalLettersSummariesMapper.mapIn ...");
        if(input == null){
            return null;
        }
        FormatoRIMRF40 format = new FormatoRIMRF40();
        format.setCodcent(input.getBusinessId());
        return format;
    }

    @Override
    public List<HistoricalLettersSummaries> mapOut1(final FormatoRIMRF41 formatoRIMRF41) {
        LOG.info("... called method TxListHistoricalLettersSummariesMapper.mapOut1 ...");

        List<HistoricalLettersSummaries> list = new ArrayList<>();
        HistoricalLettersSummaries historicalLettersSummaries = new HistoricalLettersSummaries();
        historicalLettersSummaries.setId(formatoRIMRF41.getId());
        historicalLettersSummaries.setName(formatoRIMRF41.getEfedes());
        list.add(historicalLettersSummaries);
        return list;
    }

    @Override
    public List<HistoricalLettersSummaries> mapOut2(final FormatoRIMRF42 formatoRIMRF42, final List<HistoricalLettersSummaries> listOut) {
        LOG.info("... called method TxListHistoricalLettersSummariesMapper.mapOut2 ...");
        List<HistoricalLettersSummaries> historicalLettersSummaries = new ArrayList<>();
        HistoricalLettersSummaries historicalLettersSummarie= new HistoricalLettersSummaries();
        HistoricalParameters historicalParameters = new HistoricalParameters();
        historicalLettersSummarie.setHistoricalParameters(new ArrayList<>());

        historicalParameters.setPercentage(formatoRIMRF42.getPorefe());
        historicalParameters.setInformationPeriod(mapOutInformationPeriod(formatoRIMRF42.getPerefel(), formatoRIMRF42.getUniefel()));
        historicalLettersSummarie.getHistoricalParameters().add(historicalParameters);
        historicalLettersSummaries.add(historicalLettersSummarie);
        return historicalLettersSummaries;
    }

    private InformationPeriod mapOutInformationPeriod(final Integer perefel, final String uniefel) {
        InformationPeriod informationPeriod = new InformationPeriod();
        informationPeriod.setNumber(perefel);
        informationPeriod.setUnit(uniefel);
        return informationPeriod;
    }

}
