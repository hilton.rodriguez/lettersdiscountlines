package com.bbva.pzic.lettersdiscountlines.dao.tx.mapper;

import com.bbva.pzic.lettersdiscountlines.business.dto.InputListHistoricalLettersSummaries;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF40;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF41;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF42;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;

import java.util.List;

public interface ITxListHistoricalLettersSummariesMapper {

    FormatoRIMRF40 mapIn(InputListHistoricalLettersSummaries input);

    List<HistoricalLettersSummaries> mapOut1(FormatoRIMRF41 formatoRIMRF41);

    List<HistoricalLettersSummaries> mapOut2(FormatoRIMRF42 formatoRIMRF42, List<HistoricalLettersSummaries> listOut);

}
