package com.bbva.pzic.lettersdiscountlines.dao;

import com.bbva.pzic.lettersdiscountlines.business.dto.InputListHistoricalLettersSummaries;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;

import java.util.List;

public interface ILettersDiscountLinesDAO {
    List<HistoricalLettersSummaries> lisHistoricalLettersSummaries (InputListHistoricalLettersSummaries input);
}
