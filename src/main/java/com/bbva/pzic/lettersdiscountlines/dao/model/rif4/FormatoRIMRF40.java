package com.bbva.pzic.lettersdiscountlines.dao.model.rif4;


import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * Formato de datos <code>RIMRF40</code> de la transacci&oacute;n <code>RIF4</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "RIMRF40")
@RooJavaBean
@RooSerializable
public class FormatoRIMRF40 {

	/**
	 * <p>Campo <code>CODCENT</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@DatoAuditable(omitir = true)
	@Campo(indice = 1, nombre = "CODCENT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String codcent;
	
}