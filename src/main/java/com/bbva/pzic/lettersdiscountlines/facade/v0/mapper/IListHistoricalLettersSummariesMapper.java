package com.bbva.pzic.lettersdiscountlines.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.lettersdiscountlines.business.dto.InputListHistoricalLettersSummaries;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;

import java.util.List;

public interface IListHistoricalLettersSummariesMapper {

    InputListHistoricalLettersSummaries mapIn(String businessId);

    ServiceResponse<List<HistoricalLettersSummaries>> mapOut(List<HistoricalLettersSummaries> historicalLettersSummaries);
}
