package com.bbva.pzic.lettersdiscountlines.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.lettersdiscountlines.business.dto.InputListHistoricalLettersSummaries;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;
import com.bbva.pzic.lettersdiscountlines.facade.v0.mapper.IListHistoricalLettersSummariesMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ListHistoricalLettersSummariesMapper implements IListHistoricalLettersSummariesMapper {

    private static final Log LOG = LogFactory.getLog(ListHistoricalLettersSummariesMapper.class);

    @Override
    public InputListHistoricalLettersSummaries mapIn(final String businessId) {
        LOG.info("... called method ListHistoricalLettersSummariesMapper.mapIn ...");
        if(businessId == null){
            return null;
        }
        InputListHistoricalLettersSummaries input = new InputListHistoricalLettersSummaries();
        input.setBusinessId(businessId);
        return input;
    }

    @Override
    public ServiceResponse<List<HistoricalLettersSummaries>> mapOut(final List<HistoricalLettersSummaries> historicalLettersSummaries) {
        if(CollectionUtils.isEmpty(historicalLettersSummaries)){
            return null;
        }
        return ServiceResponse.data(historicalLettersSummaries).build();
    }
}
