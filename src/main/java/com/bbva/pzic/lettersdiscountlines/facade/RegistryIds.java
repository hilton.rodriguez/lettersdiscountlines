package com.bbva.pzic.lettersdiscountlines.facade;

public class RegistryIds {

    // TO HRV declarando constante del registry id
    public static  final String SMC_REGISTRY_ID_OF_LIST_HISTORICAL_LETTERS_SUMMARIES = "SMGG20203815";

    private RegistryIds() {

    }
}
